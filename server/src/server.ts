// Rota: endereço completo da requisição
// Recurso: Qual entidade do sistema estamos acessando

// GET: buscar informações do backend
// POST: Criar uma nova informação no backend
// PUT: Atualizar uma informação existente no backend
// DELETE: Remover uma informação do backend

// Request param (obrigatório): Parâmetro que vem na própria rota que identifica um recurso
// Query param (opcional): Parâmetros que vem na própria rota, após "?", para filtros, paginação, etc.
// Request body: Parâmetros para criação/atualização de informações

import express from 'express'
import cors from 'cors'
import routes from './routes'
import path from 'path'

const app = express()

app.use(cors())
app.use(express.json())
app.use(routes)
app.listen(3333)

app.use('/uploads', express.static(path.resolve(__dirname, '..', 'uploads')))
