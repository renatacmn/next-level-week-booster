import knex from 'knex'
import path from 'path' // Path builder. Adapta os paths para o sistema que está rodando

const connection = knex({ 
    client: 'sqlite3',
    connection: {
        filename: path.resolve(__dirname, 'database.sqlite')
    },
    useNullAsDefault: true
})

export default connection

// Knex Migrations = Histórico do banco de dados

